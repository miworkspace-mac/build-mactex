#!/bin/sh
defaults write /Library/Preferences/TeXShop "SUAutomaticallyUpdate" 0
defaults write /Library/Preferences/TeXShop "SUEnableAutomaticChecks" 0
defaults write /Library/Preferences/com.googlecode.mactlmgr.tlu "SUAutomaticallyUpdate" 0
defaults write /Library/Preferences/com.googlecode.mactlmgr.tlu "SUEnableAutomaticChecks" 0
defaults write /Library/Preferences/fr.chachatelier.pierre.LaTeXiT "SUAutomaticallyUpdate" 0
defaults write /Library/Preferences/fr.chachatelier.pierre.LaTeXiT "SUEnableAutomaticChecks" 0
defaults write /Library/Preferences/edu.ucsd.cs.mmccrack.bibdesk "SUAutomaticallyUpdate" 0
defaults write /Library/Preferences/edu.ucsd.cs.mmccrack.bibdesk "SUEnableAutomaticChecks" 0
defaults write /Library/Preferences/com.googlecode.mactlmgr.tlu "TLMDisableGPGAlertPreferenceKey" -int 2018
