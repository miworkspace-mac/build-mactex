#!/bin/bash
# ImageMagick may mess up /usr/local/lib perms, so we fix them here
/bin/chmod 755 /usr/local/lib

/usr/local/texlive/2015/bin/x86_64-darwin/tlmgr paper letter
# /usr/local/texlive/2012/bin/x86_64-darwin/tlmgr update --self
# /usr/local/texlive/2012/bin/x86_64-darwin/tlmgr update --all