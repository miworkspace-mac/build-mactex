#!/bin/bash -ex

# CONFIG
prefix="MacTeX"
suffix=""
munki_package_name="MacTeX2018"
display_name="MacTeX 2018"
description="MacTeX is a redistribution of TeX Live, a typesetting environment."

# download it (-L: follow redirects)
curl -L -o MacTeX.pkg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' http://mirror.ctan.org/systems/mac/mactex/MacTeX.pkg

# make DMG from the inner prefpane
#mkdir pkgfldr
#mv MacTeX.pkg pkgfldr
#hdiutil create -size 4g -srcfolder pkgfldr -format UDZO -o app.dmg

## EXAMPLE: unpacking a flat package to find appropriate things
## Mount disk image on temp space
#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`

## Unpack
/usr/sbin/pkgutil --expand "MacTeX.pkg" pkg
mkdir build-root
(cd build-root; pax -rz -f ../pkg/applications.pkg/Payload; pax -rz -f ../pkg/root.pkg/Payload; pax -rz -f ../pkg/local.pkg/Payload)
#hdiutil detach "${mountpoint}"

path="build-root/TeX/"

mkdir -p build-apps/Applications/TeX

mv "${path}"/LaTeXiT* build-apps/Applications/TeX
mv "${path}"/Excalibur* build-apps/Applications/TeX
mv "${path}"/TeXShop* build-apps/Applications/TeX
mv "${path}"/BibDesk* build-apps/Applications/TeX
# mv "${path}"/FixMacTeX2014* build-apps/Applications/TeX

## Find all the appropriate apps, etc, and then turn that into -f's -name 'Excalibur.app' -name 'LaTeXiT.app'
key_files=`find build-apps -name '*.app' -or -name '*.plugin' -or -name '*.framework' -or -name '*.pkg' -maxdepth 4 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
#echo /usr/local/munki/makepkginfo -m go-w -g admin -o root --preinstall_script=preinstall.sh app.dmg ${key_files} | /bin/bash > app.plist
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root --postinstall_script=postinstall.sh MacTeX.pkg ${key_files} | /bin/bash > app.plist


## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-apps//' app.plist
## END EXAMPLE

# Version piece-together
version_check=`grep org.tug.mactex.texlive app.plist`
echo "${version_check}" > version1.plist
version1=`cut -c 34-37 version1.plist`
version2=`/usr/libexec/PlistBuddy -c "print :receipts:1:version" app.plist`
version="${version1}.${version2}"

plist=`pwd`/app.plist

# Cleanup of extra plist
rm -rf version1.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version1}-${version2}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" description -string "${description}"

# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv MacTeX.pkg   ${prefix}-${version1}-${version2}${suffix}.pkg
mv app.plist ${prefix}-${version1}-${version2}${suffix}.plist

